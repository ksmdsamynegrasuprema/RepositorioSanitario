#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

struct tecnologiaData{
unsigned int registro;
char nombre[40];
char finalidad[40];
char categoria[40];
unsigned int estado;
char motivo[100];
};
enum estado {inactivo=1,activo};

void registrarTecnologia(FILE *fPtr);
//void consultarTecnologia(FILE *fPtr);
void actualizarDatos(FILE *fPtr);
void darDeBaja(FILE *fPtr);
void generarReporte(FILE *fPtr);
unsigned int eleccion();

int main()
{
    FILE *cfPtr;
    unsigned int opcion;
    if ( ( cfPtr = fopen( "tecnologia.dat", "rb+") ) == NULL )
    {
        puts( "El archivo no puede ser abierto" );
    }
    else
    {
        while ( ( opcion = eleccion() ) !=6 )
        {
            switch ( opcion )
            {
            case 1:
                registrarTecnologia( cfPtr );
                break;
            case 2:
                //consultarTecnologia( cfPtr );
                break;
            case 3:
                actualizarDatos( cfPtr );
                break;
            case 4:
                darDeBaja( cfPtr );
                break;
            case 5:
               generarReporte(cfPtr);
               break;
            default:
                puts( "Eleccion incorrecta" );
                break;
            }
        }
        fclose( cfPtr );
    }
    return 0;
}
void registrarTecnologia(FILE *fPtr)
{
    struct tecnologiaData tecnologia = {0,"","","",0} ;
    unsigned int registros;
    printf("Ingrese numero de registro (1-100): ");
    scanf("%d",&registros);
    fseek(fPtr,(registros-1)*sizeof(struct tecnologiaData),SEEK_SET);
    fread(&tecnologia, sizeof(struct tecnologiaData),1,fPtr);
    if(tecnologia.registro !=0)
    {
        printf("Registro #%d contiene informacion.\n",tecnologia.registro);
    }
    else
    {
        printf("Ingrese nombre, finalidad y categoria\n");
        scanf("%s%s%s",&tecnologia.nombre,&tecnologia.finalidad,&tecnologia.categoria);
        tecnologia.registro=registros;
        tecnologia.estado=activo;
        fseek(fPtr,(tecnologia.registro-1)*sizeof(struct tecnologiaData),SEEK_SET);
        fprintf(fPtr,"Registro Nombre Finalidad Categoria \n");
        fprintf(fPtr,"%-6d%-9s%-9s%-9s%-5d \n",tecnologia.registro,tecnologia.nombre,tecnologia.finalidad,tecnologia.categoria,tecnologia.estado);
    }
}
/*void cosultarTecnologia(FILE *fPtr)
{
    struct tecnologiaData tecnologia = {0,"","","",0,""} ;
    char palabraClave[20];
    printf("Ingrese una palabra clave de la tecnología sanitaria a consultar: ");
    fgets(palabraClave, 20,stdin);
    fseek(fPtr,sizeof(struct tecnologiaData),SEEK_SET);
    fread(&tecnologia,sizeof(struct tecnologiaData),1,fPtr);
    if (strcmp(palabraClave,tecnologia.nombre)==0)
    {
        printf("%-6d%-9s%-9s%-9s5-9d\n",tecnologia.registro,tecnologia.nombre,tecnologia.finalidad,tecnologia.categoria,tecnologia.estado);
    }
    else
    {
        printf("Tecnología no registrada.");
    }
    rewind(fPtr);
}
*/
void actualizarDatos(FILE *fPtr)
{
    unsigned int registro;
    int res;
    struct tecnologiaData tecnologia = {0,"","","",0};
    printf("Ingrese el numero de registro a actualizar(1-100): ");
    scanf("%d",&registro);
    fseek(fPtr,(registro-1)*sizeof(struct tecnologiaData),SEEK_SET);
    fread(&tecnologia,sizeof(struct tecnologiaData),1,fPtr);
    if(tecnologia.registro==0)
    {
        printf("No existe informacion en el registro #%d",registro);
    }
    else
    {
        printf("%-6d%-16s%-11s%-15s%-9d\n\n", tecnologia.registro,tecnologia.nombre,tecnologia.finalidad,tecnologia.categoria,tecnologia.estado);
        printf("Desea hacer un cambio.1-Nombre 2-Finalidad 3-Categoria 4-Cancelar\n\n");
        scanf("%d", &res);
            switch(res)
            {
            case 1:
                printf("Ingrese nombre nuevo: ");
                scanf("%s",tecnologia.nombre,stdin);
                fseek(fPtr,(registro-1)*sizeof(struct tecnologiaData),SEEK_SET);
                fwrite(&tecnologia,sizeof(struct tecnologiaData),1,fPtr);
                break;
            case 2:
                printf("Ingrese finalidad nueva: ");
                scanf("%s",tecnologia.finalidad,stdin);
                fseek(fPtr,(registro-1)*sizeof(struct tecnologiaData),SEEK_SET);
                fwrite(&tecnologia,sizeof(struct tecnologiaData),1,fPtr);
                break;
            case 3:
                printf("Ingrese categoria nueva: ");
                scanf("%s",tecnologia.categoria,stdin);
                fseek(fPtr,(registro-1)*sizeof(struct tecnologiaData),SEEK_SET);
                fwrite(&tecnologia,sizeof(struct tecnologiaData),1,fPtr);
                break;
            case 4:
                break;
            default:
                printf("Cambio no aceptado");
            }
    }
}
void darDeBaja(FILE *fPtr)
{
    struct tecnologiaData tecnologia = {0,"","","",0,""};
    int registros;
    printf( "Ingrese el numero de registro a dar de baja(1-100): \n" );
    scanf("%d",&registros);
    fseek( fPtr, ( registros - 1 ) * sizeof( struct tecnologiaData ),
           SEEK_SET );
    fread( &tecnologia, sizeof( struct tecnologiaData ), 1, fPtr );
    if ( tecnologia.registro == 0 )
    {
        printf( "No existe el registro #%d.\n", registros );
    }
    else
    {
        printf("Ingrese el motivo de baja: \n");
        scanf("%s",tecnologia.motivo,stdin);
        tecnologia.estado=inactivo;
        fseek( fPtr, ( registros - 1 ) * sizeof( struct tecnologiaData ),SEEK_SET );
        fwrite( &tecnologia,sizeof( struct tecnologiaData ), 1, fPtr );
        printf("Tecnologia dada de baja.");
    }
}
void generarReporte(FILE *fPtr)
{
    struct tecnologiaData tecnologia = {0,"","","",0,""};
    unsigned int respuesta;
    int resultado;
    printf("Desea generar reporte de: 1-tecnologia sanitaria activa 2- tecnologias sanitarias dadas de baja.\n\n");
    scanf("%u",&respuesta);
    rewind(fPtr);
    while ( !feof( fPtr ) )
    {
       resultado=fread(&tecnologia,sizeof(struct tecnologiaData),1,fPtr);
       if (resultado != 0 && tecnologia.registro != 0)
       {
         if (respuesta==1 && (tecnologia.estado==activo))
         {
            printf("%-6d%-9s%-9s%-9s%-5d\n",tecnologia.registro,tecnologia.nombre,tecnologia.finalidad,tecnologia.categoria,tecnologia.estado);

        }else if (respuesta==2 &&(tecnologia.estado==inactivo))
        {
            printf("%-6d%-9s%-9s%-9s%-5d%-9s\n",tecnologia.registro,tecnologia.nombre,tecnologia.finalidad,tecnologia.categoria,tecnologia.estado,tecnologia.motivo);
        }
       }
    }
}
unsigned int eleccion(void)
{
unsigned int menuEleccion;
    printf( "%s", "\nEscoja una opcion\n"
            "1 - Registrar nueva tecnologia sanitaria\n"
            "2 - Consultar tecnologia sanitaria\n"
            "3 - Actualizar tecnologia sanitaria\n"
            "4 - Dar de baja tecnologia sanitaria\n"
            "5 - Generar reporte de tecnologia sanitaria\n"
            "6 - Salir\n\n" );

    scanf( "%u", &menuEleccion );
    return menuEleccion;
}
