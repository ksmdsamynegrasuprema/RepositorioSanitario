#include <stdio.h>
#include <stdlib.h>
struct tecnologiaData
{
    unsigned int registro;
    char nombre[40];
    char categoria[40];
    char finalidad [40];
    unsigned int estado;
    char motivo[100];
};
int main()
{
    unsigned int i;
    struct tecnologiaData tecnologia = {0,"","","",0,""};
    FILE *cfPtr;
    if((cfPtr = fopen("tecnologia.dat","wb"))==NULL)
    {
        puts("No es posible abrir el archivo.");
    }
    else{
        for(i=1;i<=100; ++i)
        {
            fwrite(&tecnologia, sizeof(struct tecnologiaData),1,cfPtr);
        }
    }
    fclose(cfPtr);
    printf("Archivo creado con exito");
    return 0;
}
